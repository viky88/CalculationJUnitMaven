package src.com.company.calculation;

import org.junit.Before;
import org.junit.Test;
import src.com.company.operations.*;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashMap;

import static org.junit.Assert.*;


public class SimpleCalcTest {
    SimpleCalc simpleCalc;
    SimpleCalc simpleCalcDec;

    @Before
    public void setUp() throws CloneNotSupportedException {
        String[] args = {"2", "+", "2", "*", "4"};
        String[] args1 = {"2.2", "+", "2.6", "*", "4.5"};
        simpleCalc = new SimpleCalc(args, BigInteger.class);
        simpleCalcDec = new SimpleCalc(args1, BigDecimal.class);
        HashMap<ConstOperation, Operations> map = new HashMap<>();
        map.put(ConstOperation.PLUS, Plus.createInstance());
        map.put(ConstOperation.MINUS, Minus.createInstance());
        map.put(ConstOperation.MULTIPLY, Multiply.createInstance());
        map.put(ConstOperation.DIVIDE, Divide.createInstance());
        simpleCalc.setOperations(map);
        simpleCalcDec.setOperations(map);
    }
    @Test
    public void getResult() throws Exception {

        assertEquals(new BigDecimal(16), simpleCalc.getResult());
        assertEquals(new BigDecimal("21.60"), simpleCalcDec.getResult());
    }

}