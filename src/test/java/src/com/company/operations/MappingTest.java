package src.com.company.operations;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;


public class MappingTest {

    private Operations plus = new Plus();
    private Operations minus = new Minus();
    private Operations multiply = new Multiply();
    private Operations divide = new Divide();
    private String symbolPlus = "Plus";
    private String symbolMinus = "Minus";
    private String symbolMultiply = "Multiply";
    private String symbolDivide = "Divide";


    @Test
    public void getSymbol() throws Exception {

        assertEquals(symbolPlus.toLowerCase(), plus.getSymbol());
        assertEquals(symbolMinus.toLowerCase(), minus.getSymbol());
        assertEquals(symbolMultiply.toLowerCase(), multiply.getSymbol());
        assertEquals(symbolDivide.toLowerCase(), divide.getSymbol());
        assertNotSame(symbolDivide.toLowerCase(), plus.getSymbol());
    }

    @Test
    public void isMySymbol() throws Exception {
        assertTrue(plus.isMySymbol("+"));
        assertTrue(minus.isMySymbol("-"));
        assertTrue(multiply.isMySymbol("*"));
        assertTrue(divide.isMySymbol("/"));
    }

}