package src.com.company.operations;

import org.junit.Test;

import java.math.BigDecimal;
import java.math.BigInteger;

import static org.junit.Assert.*;


public class DivideTest {

    @Test //(expected = ArithmeticException.class)
    public void execute() throws Exception {
        Divide divide = new Divide();
        assertEquals(new BigInteger("1"), divide.execute(BigInteger.class, 2, 2));
        assertEquals(new BigInteger("4"), divide.execute(BigInteger.class, 4, 1));
        assertEquals(new BigInteger("-1"), divide.execute(BigInteger.class, -2, 2));
        assertEquals(new BigInteger("-2"), divide.execute(BigInteger.class, -4, 2));
        assertNotSame(new BigInteger("3"), divide.execute(BigInteger.class, 3, 0));
        assertNotSame(new BigDecimal("3.00"), divide.execute(BigDecimal.class, 3.0, 0.0));
        assertEquals(new BigDecimal("6.00"), divide.execute(BigDecimal.class, 3.0, 0.5));
        assertEquals(new BigDecimal("1.50"), divide.execute(BigInteger.class, 3, 2));
    }

    @Test
    public void createInstance() throws Exception {
        Operations o = Divide.createInstance();
        assertNotSame(o, Divide.createInstance());
    }

}