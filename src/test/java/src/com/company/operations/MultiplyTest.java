package src.com.company.operations;

import org.junit.Test;

import java.math.BigDecimal;
import java.math.BigInteger;

import static org.junit.Assert.*;


public class MultiplyTest {
    @Test
    public void execute() throws Exception {
        Multiply multiply = new Multiply();
        assertEquals(new BigInteger("4"), multiply.execute(BigInteger.class, 2, 2));
        assertEquals(new BigInteger("-4"), multiply.execute(BigInteger.class, -2, 2));
        assertEquals(new BigInteger("-8"), multiply.execute(BigInteger.class, -4, 2));
        assertEquals(new BigInteger("0"), multiply.execute(BigInteger.class, 3, 0));
        assertEquals(new BigDecimal("0.00"), multiply.execute(BigDecimal.class, 3.0, 0.0));
        assertEquals(new BigDecimal("2.00"), multiply.execute(BigDecimal.class, 4.0, 0.5));
        assertEquals(new BigDecimal("9.00"), multiply.execute(BigDecimal.class, 6.0, 1.5));
    }

    @Test
    public void createInstance() throws Exception {
        Operations o = Multiply.createInstance();
        assertNotSame(o, Multiply.createInstance());
    }

}