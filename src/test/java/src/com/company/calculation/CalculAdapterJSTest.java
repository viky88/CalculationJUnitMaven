package src.com.company.calculation;

import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;


public class CalculAdapterJSTest {
    CalculAdapterJS calculAdapterJS;

    @Before
    public void setUp() {
        String[] args = {"2.2", "+", "2.6", "*", "4.5", "/", "3.3"};
        calculAdapterJS = new CalculAdapterJS(args);
    }
    @Test
    public void getResult() throws Exception {
        assertEquals(new BigDecimal("5.745454545454546"), calculAdapterJS.getResult());

    }

}