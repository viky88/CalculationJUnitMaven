package src.com.company.operations;

import org.junit.Test;

import java.math.BigDecimal;
import java.math.BigInteger;

import static org.junit.Assert.*;

public class MinusTest {
    Minus minus = new Minus();

    @Test
    public void execute() throws Exception {
        assertEquals(new BigInteger("-1"), minus.execute(BigInteger.class, 2, 3));
        assertEquals(new BigInteger("0"), minus.execute(BigInteger.class, 2, 2));
        assertEquals(new BigInteger("-2"), minus.execute(BigInteger.class, 0, 2));
        assertEquals(new BigInteger("3"), minus.execute(BigInteger.class, 3, 0));
        assertEquals(new BigDecimal("3.0"), minus.execute(BigDecimal.class, 3.0, 0.0));
        assertEquals(new BigDecimal("2.5"), minus.execute(BigDecimal.class, 3.0, 0.5));
        assertEquals(new BigDecimal("-5.5"), minus.execute(BigDecimal.class, 1.1, 6.6));
    }

    @Test
    public void createInstance() throws Exception {
        Operations m = Minus.createInstance();
        assertNotSame(m, Minus.createInstance());
    }

}