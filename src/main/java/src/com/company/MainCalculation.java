package src.com.company;

import src.com.company.calculation.Calcul;
import src.com.company.calculation.CalculationFacade;

import static src.com.company.calculation.CalculationFacade.startCalculate;


public class MainCalculation {

    static Calcul calc;

    public static void main(String[] args) {


        try {
            CalculationFacade calculationFacade = new CalculationFacade(args);
            startCalculate(args);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
