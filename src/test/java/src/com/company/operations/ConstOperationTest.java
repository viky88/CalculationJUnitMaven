package src.com.company.operations;

import org.junit.Test;

import static org.junit.Assert.*;


public class ConstOperationTest {

    String plusSymb = "plus";
    String minusSymb = "minus";
    String multiplySymb = "multiply";
    String divideSymb = "divide";

    @Test
    public void getSymbol() throws Exception {
        assertEquals(plusSymb.toUpperCase(), ConstOperation.PLUS.name());
        assertEquals(minusSymb.toUpperCase(), ConstOperation.MINUS.name());
        assertEquals(multiplySymb.toUpperCase(), ConstOperation.MULTIPLY.name());
        assertEquals(divideSymb.toUpperCase(), ConstOperation.DIVIDE.name());
    }

    @Test
    public void getEnumByString() throws Exception {
        assertEquals(plusSymb.toUpperCase(), ConstOperation.getEnumByString("+"));
        assertEquals(minusSymb.toUpperCase(), ConstOperation.getEnumByString("-"));
        assertEquals(multiplySymb.toUpperCase(), ConstOperation.getEnumByString("*"));
        assertEquals(divideSymb.toUpperCase(), ConstOperation.getEnumByString("/"));
    }

}