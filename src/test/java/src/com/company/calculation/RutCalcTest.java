package src.com.company.calculation;

import org.junit.Before;
import org.junit.Test;
import src.com.company.operations.*;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashMap;

import static org.junit.Assert.*;


public class RutCalcTest {
    RutCalc rutCalcInt;
    RutCalc rutCalcDecim;

    @Before
    public void setUp() throws CloneNotSupportedException {
        String[] args = {"2", "+", "2", "*", "(", "4", "/", "2", ")"};
        String[] args1 = {"2.0", "+", "2.0", "*", "(", "5.0", "/", "2.0", ")"};
        rutCalcInt = new RutCalc(args, BigInteger.class);
        rutCalcDecim = new RutCalc(args1, BigDecimal.class);
        HashMap<ConstOperation, Operations> map = new HashMap<>();
        map.put(ConstOperation.PLUS, Plus.createInstance());
        map.put(ConstOperation.MINUS, Minus.createInstance());
        map.put(ConstOperation.MULTIPLY, Multiply.createInstance());
        map.put(ConstOperation.DIVIDE, Divide.createInstance());
        rutCalcInt.setOperations(map);
        rutCalcDecim.setOperations(map);
    }

    @Test
    public void getResult() throws Exception {
        assertEquals(new BigDecimal(8), rutCalcInt.getResult());
        assertEquals(new BigDecimal("10.00"), rutCalcDecim.getResult());
    }

}