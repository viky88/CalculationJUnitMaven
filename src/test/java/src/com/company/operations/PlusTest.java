package src.com.company.operations;

import org.junit.Test;

import java.math.BigDecimal;
import java.math.BigInteger;

import static org.junit.Assert.*;


public class PlusTest {


    @Test
    public void execute() {
        Plus plus = new Plus();
        assertEquals(new BigInteger("4"), plus.execute(BigInteger.class, 2, 2));
        assertEquals(new BigInteger("0"), plus.execute(BigInteger.class, -2, 2));
        assertEquals(new BigInteger("-2"), plus.execute(BigInteger.class, -4, 2));
        assertEquals(new BigInteger("3"), plus.execute(BigInteger.class, 3, 0));
        assertEquals(new BigDecimal("3.0"), plus.execute(BigDecimal.class, 3.0, 0.0));
        assertEquals(new BigDecimal("3.5"), plus.execute(BigDecimal.class, 3.0, 0.5));
        assertEquals(new BigDecimal("7.7"), plus.execute(BigDecimal.class, 1.1, 6.6));
    }

    @Test
    public void createInstance() throws Exception {
        Operations m = Plus.createInstance();
        assertNotSame(m, Plus.createInstance());
    }

}